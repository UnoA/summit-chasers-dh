﻿using UnityEngine;
using System.Collections;

public class S_SwapScene : MonoBehaviour {

    private bool playerEnter;
    private bool player2Enter;

    void OnTriggerEnter2D(Collider2D player)
    {
        if (player.gameObject.CompareTag("Player"))
        {
            playerEnter = true;
           // Destroy(player.gameObject);

        }
        if (player.gameObject.CompareTag("Player 2"))
        {
            player2Enter = true;
            //Destroy(player.gameObject);
        }


        if (playerEnter == true && player2Enter == true)
        {
            
            StartCoroutine(Changelevel());
        }
    }

  IEnumerator Changelevel()
    {
        Debug.Log("col2");
        float fadeTime = GameObject.Find("Fade").GetComponent<S_Fade>().beginFade(1);
        yield return new WaitForSeconds(fadeTime);
        Application.LoadLevel(Application.loadedLevel+1);

    }
}
