using UnityEngine;
using System.Collections;
public class S_ScreenShake : MonoBehaviour
{
    public float shakeAmount = 0.7f;
    public float decreaseFactor = 1.0f;
	private bool isShake;
    private float duration;

    void Update()
    {
		if (isShake)
        {
            if (duration > 0.0f)
            {
                gameObject.transform.position = new Vector3(Random.insideUnitCircle.x * shakeAmount, Random.insideUnitCircle.y * shakeAmount, gameObject.transform.position.z);
                duration -= Time.deltaTime * decreaseFactor;
            }
            else
            {
                duration = 0.0f;
                isShake = false;
            }
		}
    }
	void setShake()
	{
		isShake = true;
	}
}
