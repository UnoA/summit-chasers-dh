﻿using UnityEngine;
using System.Collections;
public class S_Menu : MonoBehaviour {
    public GameObject P1;
    public GameObject P2;
    public GameObject P1Particles;
    public GameObject P2Particles;
	public float P1ParticleYAxis;
	public float P2ParticleYAxis;
    private bool P1Ready = false;
    private bool P2Ready = false;

	// Use this for initialization
	void Start () {
        P1.GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, 0);
        P2.GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, 0);
    	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Joystick1Button0))
        {
            P1Ready = !P1Ready;
            Instantiate(P1Particles, new Vector2(P1.transform.position.x, P1ParticleYAxis), Quaternion.Euler(0, 0, 0));
        }
        if (Input.GetKeyDown(KeyCode.Joystick2Button0))
        {
            P2Ready = !P2Ready;
            Instantiate(P2Particles, new Vector2(P2.transform.position.x, P2ParticleYAxis), Quaternion.Euler(0, 0, 0));
        }
        if (P1Ready)
        {
            P1.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
        }
        else
        {
            P1.GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, 0);
        }
        if (P2Ready)
        {
            P2.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
        }
        else
        {
            P2.GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, 0);
        }
        if (P1Ready && P2Ready)
        {
            //Changelevel;
			Application.LoadLevel(Application.loadedLevel+1);
        }
	}
}
