﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class S_Pause : MonoBehaviour {
    bool paused;
    public Texture2D texture;
    public GameObject PauseObject;
    public Image P1;
    public Image P2;
    private bool P1Ready;
    private bool P2Ready;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        //if (!GameObject.Find("StartTimer") && !GameObject.FindGameObjectWithTag("GuiText"))
        if (Input.GetKeyDown(KeyCode.Joystick1Button7) || Input.GetKeyDown(KeyCode.Joystick2Button7))
        {
            if (Time.timeScale == 1)
                Time.timeScale = 0;
            else
                Time.timeScale = 1;
            paused = !paused;
        }
        if (paused)
        {
            PauseObject.SetActive(true);
            if (Input.GetKeyDown(KeyCode.Joystick1Button0))
            {
                if(!P1Ready)
                    GetComponent<AudioSource>().Play();

                P1Ready = !P1Ready;
            }
            if (Input.GetKeyDown(KeyCode.Joystick2Button0))
            {
                if(!P2Ready)
                    GetComponent<AudioSource>().Play();

                P2Ready = !P2Ready;
            }

            if (P1Ready && P2Ready)
            {
                Time.timeScale = 1;
                Application.LoadLevel(0);
            }

                P1.gameObject.SetActive(P1Ready);
                P2.gameObject.SetActive(P2Ready);
            
        }
        if (!paused)
        {
            PauseObject.SetActive(false);
            P2Ready = false;
            P1Ready = false;
        }
        
	}
    //void OnGUI()
    //{
    //    GUI.color = new Color(0,0,0,1);
        
    //    if (paused)
    //    {
    //        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), texture);
    //        GUI.skin.label.fontSize = 100;
    //        GUI.color = new Color(1,1,1,1);            
    //        GUI.skin.label.font = m_font;
    //        GUI.Label(new Rect(Screen.width / 2 -100, Screen.height / 2 - 400, 600, 600), "Paused");
    //        GUI.Label(new Rect(Screen.width / 2 - 290, Screen.height / 2 + 300, 1000, 600), "press A to quit");

    //        GUI.DrawTexture(new Rect(Screen.width/2 - 165, Screen.height/2 -256, 387,591), checkPoint);

    //        if (P1Ready)
    //        {
    //            GUI.DrawTexture(new Rect(Screen.width / 2 - 114, Screen.height / 2 - 186, 256, 256), P1);

    //        }

    //        if (P2Ready)
    //        {
    //            GUI.DrawTexture(new Rect(Screen.width / 2 -114, Screen.height / 2 - 183, 256,256), P2);

    //        }
    //    }
    //}
}
