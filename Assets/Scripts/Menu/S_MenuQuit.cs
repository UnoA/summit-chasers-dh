﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class S_MenuQuit : MonoBehaviour
{
    float timer;

    void Update()
    {

        if (Input.GetKey(KeyCode.Joystick1Button7) || Input.GetKey(KeyCode.Joystick2Button7))
        {
            timer += Time.deltaTime;

            if (timer >= 1)
            {
                Application.Quit();
            }
        }
        else{
            timer = 0;
        }
    }
}