﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class S_Levelselection : MonoBehaviour {
    private float Xon;
    private float Yon;
    [Range(5,25)]
    public float Spacing = 25;
    [Range(1, 5)]
    public float PopAmount = 1.25f;
    private float SelectionTimer = 0;
    private float SpamTimer;
    private Transform m_transform;
    [HideInInspector]
    public List<GameObject> Levels = new List<GameObject>();
    private List<SpriteRenderer> ChildList = new List<SpriteRenderer>();
    private string currentName;
    private GameObject OtherObject;

    //Icon Bounce variables
    private float bounceTimer;
    private bool Bounce;
    //Level selection
    private int worldSelected;
    private int Selection = 0;
    enum Objects { World, Level };
    Objects ObjectType;
    //Level selection sprites
    public Sprite Forest1;
    public Sprite Forest2;
    public Sprite Forest3;
    public Sprite Ruins1;
    public Sprite Ruins2;
    public Sprite Ruins3;
    public Sprite Peak1;
    public Sprite Peak2;
    public Sprite Peak3;
	public Sprite GuiSprite1;
	public Sprite GuiSprite2;
	public Sprite GuiSprite3;
	public Sprite GuiSprite4;
	public Sprite GuiSprite5;
	public Sprite GuiSprite6;
	public Sprite GuiSprite7;
	public Sprite GuiSprite8;
	public Sprite GuiSprite9;
    private GameObject LevelSelect1;
    private GameObject LevelSelect2;
    private GameObject LevelSelect3;
    public AudioClip selectSFX;
    // Use this for initialization
    void Start () {
        if (gameObject.name == "World")
        {
            ObjectType = Objects.World;
            OtherObject = GameObject.Find("Level");
			OtherObject.SetActive(false);
        }
        if (gameObject.name == "Level")
        {
            ObjectType = Objects.Level;
            OtherObject = GameObject.Find("World");
            LevelSelect1 = GameObject.Find("GUI_1");
            LevelSelect2 = GameObject.Find("GUI_2");
            LevelSelect3 = GameObject.Find("GUI_3");


        }
        m_transform = GetComponent<Transform>();

        for (int i = 0; i < transform.childCount; i++)
        {
            Levels.Add(transform.GetChild(i).gameObject);
            Levels[i].transform.localScale = new Vector3(1.0f, 1.0f, 0);
            Levels[i].transform.localPosition = new Vector3(i * Spacing, 0, 10);
            for(int j = 0; j <Levels[i].transform.childCount; j++)
            {
                ChildList.Add(Levels[i].transform.GetChild(j).GetComponentInChildren<SpriteRenderer>());
            }
            
        }

    }
    //JERRY EXEMPEL
    //public bool TEST = false;
    //private IEnumerator Test_Function(float p_fDuration)
    //{
    //    Debug.Log("TestFunction Started");
    //    float t = 0.0f;
    //    while(t <= p_fDuration)
    //    {
    //        t += Time.deltaTime;
    //        // x going to move from 3 to 9, diff 6, när tiden är 50% av duration ska vi vara på pos 6
    //        // lerp( from, to,
    //        yield return new WaitForEndOfFrame();
    //    }
        
        
    //    Debug.Log("TestFunction Ended");
    //    yield return new WaitForSeconds(1.0f);
    //    this.StartCoroutine(Test_Function(1.0f));
    //}
	void Update () {

        Xon = (Input.GetAxis("Joy X") * 10);
        SelectionTimer += Time.deltaTime;
        SpamTimer += Time.deltaTime;

        for (int i = 0; i < Levels.Count; i++)
        {
            Levels[i].GetComponent<SpriteRenderer>().enabled = true;
        }
        for(int i = 0; i < ChildList.Count; i++)
        {
            ChildList[i].enabled = true;
        }

        if(Input.GetKeyDown(KeyCode.RightArrow) || Xon > 0.5f)
        {
            SelectionTimer += Time.deltaTime;
            if (SelectionTimer > 0.5f)
            {
                SelectionTimer = 0;
                if (Selection < Levels.Count-1)
                {
                    Selection += 1;
                    for(int i = 0; i < Levels.Count; i++)
                    {
                        Levels[i].transform.localScale = new Vector3(1.0f, 1.0f, 0);
                        Levels[i].transform.localRotation = Quaternion.Euler(Levels[Selection].transform.localRotation.x, 0, Levels[Selection].transform.localRotation.z);
                    }
                    Bounce = true;


                }
            }
        }
            if (Input.GetKeyDown(KeyCode.LeftArrow) || Xon < -0.5f)
            {
            SelectionTimer += Time.deltaTime;
            if(SelectionTimer > 0.5f)
            {
                SelectionTimer = 0;
                if (Selection > 0)
                {
                    Selection -= 1;
                    for (int i = 0; i < Levels.Count; i++)
                    {

                        Levels[i].transform.localScale = new Vector3(1.0f, 1.0f, 0);

                        Levels[i].transform.localRotation = Quaternion.Euler(Levels[Selection].transform.localRotation.x, 0, Levels[Selection].transform.localRotation.z);
                    }
                    Bounce = true;

                }
            }
        }

        MoveCamera();
        CurrentLevelPop();
        //VisibleObjects();
        //iconBounce();

        if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.Joystick1Button0))
        {
            if (SpamTimer > 0.50f)
            {
                SpamTimer = 0;

                if (ObjectType == Objects.Level)
                {
                    GoToLevel();
                    AudioSource.PlayClipAtPoint(selectSFX, new Vector3(0, 0, 0));
                }
                if (ObjectType == Objects.World)
                {
                    GoToLevelSelection();
                    AudioSource.PlayClipAtPoint(selectSFX, new Vector3(0, 0, 0));
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.B) || Input.GetKeyDown(KeyCode.Joystick1Button1))
        {
            if (SpamTimer > 0.50f)
            {
                SpamTimer = 0;
                if (ObjectType == Objects.Level)
                {
                    GoToWorld();
                }
                if (ObjectType == Objects.World)
                {
                    GoToMenu();
                }
            }
        }
        
     }
    void MoveCamera()
    {
       
        //m_transform.position = new Vector3(Selection * 10, m_transform.position.y, m_transform.position.z);
        m_transform.position = Vector3.Lerp(new Vector3(Selection * -Spacing * m_transform.localScale.x, 0,0), m_transform.position, Time.deltaTime * 50);
		//m_transform.position = new Vector3(Selection * -Spacing * m_transform.localScale.x, 0,0);
    }
    void CurrentLevelPop()
    {
        
        Levels[Selection].transform.localScale = new Vector3(PopAmount, PopAmount, 0);
        //Levels[Selection].transform.position = new Vector3(Levels[Selection].transform.position.x, Levels[Selection].transform.position.y, 2);
        //if (Selection != 0)
        //{
        //    Levels[Selection - 1].transform.localRotation = Quaternion.Euler(Levels[Selection-1].transform.localRotation.x, 30, Levels[Selection-1].transform.localRotation.z);
        //    //Levels[Selection - 1].transform.localScale = new Vector3(1.5f,1.5f);

        //}
        //if( Selection != Levels.Count-1)
        //{
        //    Levels[Selection + 1].transform.localRotation = Quaternion.Euler(Levels[Selection].transform.localRotation.x, -30, Levels[Selection].transform.localRotation.z);
        //    //Levels[Selection + 1].transform.localScale = new Vector3(1.5f,1.5f);

        //}

    }
    void VisibleObjects()
    {
        for (int i = 0; i < Levels.Count; i++)
        {
            Levels[i].gameObject.SetActive(false);
        }
        Levels[Selection].gameObject.SetActive(true);

        if (Selection != 0)
        {
            Levels[Selection - 1].gameObject.SetActive(true);
            //Levels[Selection - 1].transform.position = new Vector3(Levels[Selection - 1].transform.position.x + 4, Levels[Selection - 1].transform.position.y, Levels[Selection - 1].transform.position.z);
        }
        if (Selection != Levels.Count-1)
        {
            Levels[Selection + 1].gameObject.SetActive(true);
            //Levels[Selection + 1].transform.position = new Vector3(Levels[Selection + 1].transform.position.x -6, Levels[Selection + 1].transform.position.y, Levels[Selection + 1].transform.position.z);

        }
    }
    void GoToLevelSelection()
    {
        gameObject.GetComponentInParent<Transform>().position = Vector3.Lerp(gameObject.GetComponentInParent<Transform>().position, new Vector3(gameObject.GetComponentInParent<Transform>().position.x, -10, gameObject.GetComponentInParent<Transform>().position.z), Time.deltaTime * 90);
        for (int i = 0; i < Levels.Count; i++)
        {
            Levels[i].GetComponent<SpriteRenderer>().enabled = false;
        }
        for (int i = 0; i < ChildList.Count; i++)
        {
            ChildList[i].enabled = false;
        }
        if (Selection == 0) {
			OtherObject.GetComponent<S_Levelselection>().Levels[0].GetComponent<SpriteRenderer>().sprite = Forest1;
			OtherObject.GetComponent<S_Levelselection>().Levels[1].GetComponent<SpriteRenderer>().sprite = Forest2;
			OtherObject.GetComponent<S_Levelselection>().Levels[2].GetComponent<SpriteRenderer>().sprite = Forest3;
		}

        if (Selection == 1) {
			OtherObject.GetComponent<S_Levelselection>().Levels[0].GetComponent<SpriteRenderer>().sprite = Ruins1; 
			OtherObject.GetComponent<S_Levelselection>().Levels[1].GetComponent<SpriteRenderer>().sprite = Ruins2;
			OtherObject.GetComponent<S_Levelselection>().Levels[2].GetComponent<SpriteRenderer>().sprite = Ruins3;
		}

        if (Selection == 2) {
			OtherObject.GetComponent<S_Levelselection>().Levels[0].GetComponent<SpriteRenderer>().sprite = Peak1;
			OtherObject.GetComponent<S_Levelselection>().Levels[1].GetComponent<SpriteRenderer>().sprite = Peak2;
			OtherObject.GetComponent<S_Levelselection>().Levels[2].GetComponent<SpriteRenderer>().sprite = Peak3;
		}

		OtherObject.gameObject.SetActive (true);
        OtherObject.GetComponent<S_Levelselection>().enabled = true;
        OtherObject.SendMessage("WorldSelected", Selection);
		
        gameObject.GetComponent<S_Levelselection>().enabled = false;
    }
    void GoToWorld()
    {
        gameObject.GetComponentInParent<Transform>().position = Vector3.Lerp(gameObject.GetComponentInParent<Transform>().position, new Vector3(gameObject.GetComponentInParent<Transform>().position.x, -10, gameObject.GetComponentInParent<Transform>().position.z), Time.deltaTime * 90);
        for (int i = 0; i < Levels.Count; i++)
        {
            Levels[i].GetComponent<SpriteRenderer>().enabled = false;
        }
        for (int i = 0; i < ChildList.Count; i++)
        {
            ChildList[i].enabled = false;
        }
        Selection = 0;
        OtherObject.GetComponent<S_Levelselection>().enabled = true;
        gameObject.GetComponent<S_Levelselection>().enabled = false;
    }
    void GoToLevel()
    {
        if (worldSelected == 1)
            Application.LoadLevel(1+Selection + Application.loadedLevel);
        if (worldSelected == 2)
            Application.LoadLevel(1+3 + Selection + Application.loadedLevel);
        if (worldSelected == 3)
            Application.LoadLevel(1+6 + Selection + Application.loadedLevel);
    }
    void GoToMenu()
    {
        Application.LoadLevel(Application.loadedLevel - 1);
    }
    void iconBounce()
    {
        if (!Bounce)
            bounceTimer = 0;
        float shakeAmount = 0.2f;
        bounceTimer += Time.deltaTime;
        float StartPos;
        StartPos = Spacing;
        if (Bounce)
        {
            Levels[Selection].transform.position = new Vector3(Random.insideUnitCircle.x * shakeAmount, Levels[Selection].transform.position.y, Levels[Selection].transform.position.z);
        }
        if (bounceTimer > 0.4f)
        {
            Bounce = false;
            Levels[Selection].transform.position = new Vector3(StartPos, Levels[Selection].transform.position.y, Levels[Selection].transform.position.z);
        }
    }
    void WorldSelected(int world)
    {
        worldSelected = 1+world;
        SpamTimer = 0;
        SelectionTimer = 0.5f;
    }
}
