﻿using UnityEngine;
using System.Collections;

public class S_AudioManager : MonoBehaviour {

    public AudioClip[] musicTracks;
    private AudioSource source;
    
    public float fadeSpeed;

    private bool fadeUp;
    private bool fadeDown;
    private int currentLevel;

    private static S_AudioManager instance = null;
    public static S_AudioManager Instance
    {
        get { return instance; }
    }
    void Awake()
    {
        if (instance != null && instance != this)
        {
            Debug.Log("AudioManager Missing");
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
        source = GetComponent<AudioSource>();
        fadeUp = true;
        source.volume = 0.3f;
        source.clip = musicTracks[Application.loadedLevel];
        source.Play();
    }
    void OnLevelWasLoaded()
    {
        ChangeLevel();
    }

    void ChangeLevel()
    {
        if (currentLevel <= musicTracks.Length)
        {
            fadeDown = true;
        }
        else
        {
            source.Stop();
        }  
    }

    void Update()
    {
        if (fadeUp)
        {
            source.volume += 0.1f * fadeSpeed;

            if (source.volume >= 1f)
                fadeUp = false; 
        }
        if (fadeDown)
        {
            source.volume += 0.1f * -fadeSpeed;

            if (source.volume <= 0f)
            {
                fadeDown = false;
                source.clip = musicTracks[Application.loadedLevel];
                currentLevel = Application.loadedLevel;
                if (!source.isPlaying)
                    source.Play();
                fadeUp = true;
            }
                
        }
    }
}
