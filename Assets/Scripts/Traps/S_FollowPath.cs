﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class S_FollowPath : MonoBehaviour {

    public enum FollowType
    {
        MoveTowards,
        Lerp
    }

    public FollowType Type = FollowType.MoveTowards;
    public S_PlattformPath path;
    public float Speed = 1;
    public float MaxDistanceToGoal = .1f;

    private IEnumerator<Transform> m_currentPoint;

    public void Start()
    {
        if (path == null)
        {
            Debug.LogError("path cannot be null", gameObject);
            return;
        }
        m_currentPoint = path.GetPathsEnumeratos();
        m_currentPoint.MoveNext();

        if (m_currentPoint.Current == null)
            return;

        transform.position = m_currentPoint.Current.position;
    }

    public void Update()
    {

        if (m_currentPoint == null || m_currentPoint.Current == null)
            return;

        if (Type == FollowType.MoveTowards)
            transform.position = Vector3.MoveTowards(transform.position, m_currentPoint.Current.position, Time.deltaTime * Speed);
        else if (Type == FollowType.Lerp
            )
            transform.position = Vector3.Lerp(transform.position, m_currentPoint.Current.position, Time.deltaTime * Speed);

        var distanceSquared = (transform.position - m_currentPoint.Current.position).sqrMagnitude;
        if (distanceSquared < MaxDistanceToGoal * MaxDistanceToGoal)
            m_currentPoint.MoveNext();
        

    }
}
