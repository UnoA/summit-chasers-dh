﻿using UnityEngine;
using System.Collections;

public class S_PushBlockPush : MonoBehaviour {
    private float m_playerSpeed;
    [Tooltip("Launch rotation")]
    public float Angle = 270;
    [Tooltip("Launch force")]
    public float Force = 10;
	void Start () 
    {

    }
	
	// Update is called once per frame
	void Update () 
    {
        Angle = this.GetComponentInParent<Transform>().rotation.z - 90;
    }

    void OnTriggerStay2D(Collider2D coll)
    {
        if(coll.tag == "Player" || coll.tag == "Player 2")
            coll.gameObject.GetComponent<Rigidbody2D>().velocity = Quaternion.AngleAxis(270, transform.right) * transform.forward * Force * this.GetComponentInParent<S_PushBlocksMove>().force;
        

    }
}
