﻿using UnityEngine;
using System.Collections;

public class S_AnimateBreakable : MonoBehaviour {
	
	public bool isRight;
	public bool isMiddle;
	public bool isLeft;

	void Start () {
		if (isRight)
			GetComponent<Animator>().SetBool("isRight", true);
		else if (isLeft)
			GetComponent<Animator>().SetBool("isLeft", true);
	}
}
