﻿using UnityEngine;
using System.Collections;

public class S_PushBlocksMove : MonoBehaviour {
    private Transform m_transform; 
    private Vector3 currentPos;
    private Vector3 targetPos;
    private Vector3 startPos;
    public float xMove = 0;
    public float yMove = 0;
    public float timeLimit = 1.5f;
    private bool xpush;
    private bool ypush;
    private float xpushTimer;
    private float ypushTimer;
   
    [HideInInspector]
    public float force;
	// Use this for initialization
	void Start () {
        
        m_transform = GetComponent<Transform>();
        targetPos.x = m_transform.position.x + xMove;
        targetPos.y = m_transform.position.y + yMove;
        targetPos.z = m_transform.position.z;
        
        startPos = m_transform.position;
	}
	
	// Update is called once per frame
    void Update()
    {

        if (startPos.x != targetPos.x)
        {
        xpushTimer += Time.deltaTime;
			
            /////////////////////
            //X ++++++++++++++++
            ////////////////////
            if(this.GetComponentInParent<S_PushBlocksMove>().xMove > 0)
            {
             currentPos = m_transform.position;
                if (currentPos.x <= startPos.x  && xpushTimer > timeLimit)
                {
                    xpush = true;
                }
                if (currentPos.x >= targetPos.x)
                {
                    xpush = false;

                }

                if (xpush == true)
                {
                    currentPos.x += 0.25f;
                    force++;
                    xpushTimer = 0;
                }
                if (xpush == false && currentPos.x >= startPos.x)
                {
                    currentPos.x -= 0.05f;
                    force = 0;
                }
                m_transform.position = currentPos;
            }
            /////////////////////
            //X ----------------
            ////////////////////
            if (this.GetComponentInParent<S_PushBlocksMove>().xMove < 0)
            {
                currentPos = m_transform.position;
                if (currentPos.x >= startPos.x && xpushTimer > timeLimit)
                {
                    xpush = true;
                }
                if (currentPos.x <= targetPos.x)
                {
                    xpush = false;
                }

                if (xpush == true)
                {
                    currentPos.x -= 0.25f;
                    force++;
                    xpushTimer = 0;
                }
                if (xpush == false && currentPos.x <= startPos.x)
                {
                    currentPos.x += 0.05f;
                    force = 0;
                }
                m_transform.position = currentPos;
            }
        }

       if (startPos.y != targetPos.y)
       {
        ypushTimer += Time.deltaTime;
			
           /////////////////////
           //Y ++++++++++++++++
           ////////////////////
           if (this.GetComponentInParent<S_PushBlocksMove>().yMove > 0)
           {
           currentPos = m_transform.position;
           if (currentPos.y <= startPos.y && ypushTimer > timeLimit)
           {
               ypush = true;
               ypushTimer = 0;
           }
           if (currentPos.y >= targetPos.y)
           {
               ypush = false;
           }

           if (ypush == true)
           {
               currentPos.y += 0.25f;
               force++;
           }
           if (ypush == false && currentPos.y >= startPos.y)
           {
               currentPos.y -= 0.05f;
               force = 0;
           }
           m_transform.position = currentPos;
           }

           /////////////////////
           //Y ---------------
           ////////////////////
           if (this.GetComponentInParent<S_PushBlocksMove>().yMove < 0)
           {
               currentPos = m_transform.position;
               if (currentPos.y >= startPos.y && ypushTimer > timeLimit)
               {
                   ypush = true;
                   ypushTimer = 0;
               }
               if (currentPos.y <= targetPos.y)
               {
                   ypush = false;
               }

               if (ypush == true)
               {
                   currentPos.y -= 0.25f;
                   force++;
               }
               if (ypush == false && currentPos.y <= startPos.y)
               {
                   currentPos.y += 0.05f;
                   force = 0;
               }
               m_transform.position = currentPos;
           }
       }

    }
    
}
