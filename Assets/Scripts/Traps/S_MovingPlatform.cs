﻿using UnityEngine;
using System.Collections;

public class S_MovingPlatform : MonoBehaviour {
    private Transform m_transform;
    private Vector3 curDir;
    private Vector3 oldDir;
    private Vector3 randDir;
    private float timer;
    public float range = 0.1f;
	// Use this for initialization
	void Start () {
        m_transform = GetComponent<Transform>();
        curDir = m_transform.position;
        oldDir = m_transform.position;
        randDir = m_transform.position;
        randDir.y = 0.1f;
	}
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;

        if (curDir == randDir)
        {
            oldDir = randDir;
            randDir.y =range;
            timer = 0;
        }
        if (oldDir == randDir)
        {
            randDir.y = -range;
            timer = 0;
        }
        curDir = Vector3.Lerp(oldDir, randDir, timer *1);

        m_transform.position = curDir;
	}
}
