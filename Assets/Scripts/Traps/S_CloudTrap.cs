﻿using UnityEngine;
using System.Collections;

public class S_CloudTrap : MonoBehaviour {
    bool Above;
    public float Force = 10;
    private ParticleSystem m_particle;
	// Use this for initialization
	void Start () {
        m_particle = GetComponent<ParticleSystem>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player" || other.tag == "Player 2")
        {
            if (other.GetComponent<Transform>().position.y < GetComponent<Transform>().position.y)
                Above = true;
            else
                Above = false;
            m_particle.Play();
        }
    }
    void OnTriggerStay2D(Collider2D other)
    {

        if (other.tag == "Player" || other.tag == "Player 2")
        {
            if (Above)
        other.GetComponent<Rigidbody2D>().AddForce(new Vector2(0,-Force));

            else
                other.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, Force));
        }
    }
}
