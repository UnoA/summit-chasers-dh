﻿using UnityEngine;
using System.Collections;

public class S_BreakableObjects : MonoBehaviour {
    public GameObject potParticle;
    private Transform m_transform;
    private Vector3 m_startPosition;
	public AudioClip[] smashSoundEffects;
	private AudioSource m_audio;
    [Tooltip("Pot/Golem/Leaf")]
    public string breakableType;
	void Start () {
        m_transform = GetComponent<Transform>();
        m_startPosition = m_transform.position;
		m_audio = GetComponent<AudioSource>();

	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player" || other.gameObject.tag == "Player 2")
        {           
             if(smashSoundEffects.Length != 0)
			AudioSource.PlayClipAtPoint(smashSoundEffects[Random.Range(0, smashSoundEffects.Length)], transform.position);

            Instantiate(potParticle, new Vector3(m_startPosition.x, m_startPosition.y, -6), Quaternion.Euler(0, 0, 0));
            
            Destroy(this.gameObject);
            
        }

    }
}
