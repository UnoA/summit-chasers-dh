﻿using UnityEngine;
using System.Collections;

public class S_Birdie : MonoBehaviour {
    private Transform m_transform;
    private Rigidbody2D m_rigidbody;
    private Animator m_animator;
    public GameObject birdParticle;
    private Vector3 m_targetPosition;
    private Vector3 m_startPosition;
    private bool fly = false;
    private float timer;

	void Start () {
        m_transform = GetComponent<Transform>();
        m_rigidbody = GetComponent<Rigidbody2D>();
        m_animator = GetComponent<Animator>();
        m_startPosition = m_transform.position;
		m_startPosition.z = -10;
        m_targetPosition = transform.position;
        m_targetPosition.x += Random.Range(-5, 5);
        m_targetPosition.y += Random.Range(5, 10);
		m_targetPosition.z = -50;
		m_transform.position = new Vector3(m_transform.position.x, m_transform.position.y, -50); 
	}
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;
        if (fly == true)
        {
            m_transform.position = Vector2.Lerp(m_transform.position, m_targetPosition, Time.deltaTime * 1.5f);
            m_animator.SetBool("Fly", true);
			m_transform.position=new Vector3 (m_transform.position.x,m_transform.position.y,-5f);
        }
	    if (timer > 2)
        {
            fly = false;
            timer = 0;
            
        }
        if (fly == false)
        {
            m_transform.position = Vector2.Lerp(m_transform.position, m_startPosition, Time.deltaTime * 3);
            m_animator.SetBool("Fly", false);
        }


	}

    void OnTriggerEnter2D(Collider2D other)
    {
		if (other.gameObject.tag == "Player" || other.gameObject.tag == "Player 2") {
			timer = 0;
			Instantiate(birdParticle, m_transform.position, Quaternion.Euler(0, 0, 0)); 
			GetComponent <AudioSource> ().Play ();
			fly = true;
        }

    }
}
