﻿using UnityEngine;
using System.Collections;

public class S_BreakableBlocks : MonoBehaviour {
	
	public float force;
	
	public GameObject destroyedBlock;
	public GameObject damagedBlock;

	private bool isDamaged = false;
	private Animator[] m_animators;

	private AudioSource m_source;
	public AudioClip smallImpact;

    private GameObject winText;
	void Start()
	{
		m_animators = GetComponentsInChildren<Animator> ();
		m_source = GetComponent<AudioSource> ();

        winText = GameObject.FindGameObjectWithTag("GuiText");
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		if (col.gameObject.tag == "Player" || col.gameObject.tag == "Player 2") 
		{
			if (col.relativeVelocity.magnitude < force)
			{
				float pitch = Random.Range(0.5f, 1.5f);

				m_source.pitch = pitch;
				m_source.PlayOneShot(smallImpact, 1f);
			}
			if (col.relativeVelocity.magnitude > force)
			{
                gameObject.layer =0;
				Instantiate(damagedBlock, new Vector3(transform.position.x, transform.position.y, -6), transform.rotation);
				GetComponent<BoxCollider2D>().isTrigger = true;
				foreach(Animator anim in m_animators)
					anim.SetBool("isBroken", true);
			}
		}
	}
	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.gameObject.tag == "Player" || col.gameObject.tag == "Player 2") {
            if (col.gameObject.tag == "Player")
            {
                if (winText != null)
                    winText.SendMessage("Breakable", "PlayerOne");
            }
            else
            {
                if (winText != null)
                    winText.SendMessage("Breakable", "PlayerTwo");
            }
            Instantiate(destroyedBlock, new Vector3 (transform.position.x, transform.position.y, -6), transform.rotation);
			Destroy(this.gameObject);
		}
        
	}
}
 