﻿////////////////////////////////////////////////////////////////////////////////////
//                                                                                //
//  This script can be added to objects that are spawned in game from a prefab.   //
//  It destroys the entire gameobject after 4 seconds.                            //
//                                                                                //
////////////////////////////////////////////////////////////////////////////////////
using UnityEngine;
using System.Collections;

public class S_DestroyGameobject : MonoBehaviour {

	void Update()
	{
		Destroy(gameObject, 4f);
	}

}
