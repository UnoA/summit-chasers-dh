using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class S_WinBlur : MonoBehaviour {
    private float Yon;
    private float SelectionTimer;
    private float Selection;
    public GameObject WplayerWon;
    public GameObject BTM_RightHook;
    public GameObject BTM_LeftHook;
    public GameObject RightHook;
    public GameObject LeftHook;
    public GameObject NextLevel;
    public GameObject LevelSelect;
    public GameObject p_Winblur;

    public Sprite SpriteOrangeWon;
    public Sprite SpriteGreenWon;
    public Sprite SpriteNextLevel;
    public Sprite SpriteNextLevelBold;
    public Sprite SpriteLevelSelect;
    public Sprite SpriteLevelSelectBold;
    private float startTimer;
    void Start () {
        //Instantiate Camera PREFAB with Blur AT WINNING PLAYER.
        //Instantiate(p_Winblur);
        startTimer = 0;
	}
	
	void Update ()
    {
        startTimer += Time.deltaTime;
        
            Yon = (Input.GetAxis("Joy Y") * 10);
            SelectionTimer += Time.deltaTime;
            p_Winblur.GetComponent<Camera>().clearFlags = CameraClearFlags.Nothing;

            if (Yon > 0.5f && SelectionTimer > 0.25f)
            {
                Selection = 0;
            }
            if (Yon < -0.5f && SelectionTimer > 0.25f)
            {
                Selection = 1;
            }
            if (Selection == 1)
            {
                LevelSelect.GetComponent<Image>().sprite = SpriteLevelSelectBold;
                NextLevel.GetComponent<Image>().sprite = SpriteNextLevel;
                BTM_RightHook.GetComponent<Image>().enabled = true;
                BTM_LeftHook.GetComponent<Image>().enabled = true;
                RightHook.GetComponent<Image>().enabled = false;
                LeftHook.GetComponent<Image>().enabled = false;

            }
            if (Selection == 0)
            {
                LevelSelect.GetComponent<Image>().sprite = SpriteLevelSelect;
                NextLevel.GetComponent<Image>().sprite = SpriteNextLevelBold;
                BTM_RightHook.GetComponent<Image>().enabled = false;
                BTM_LeftHook.GetComponent<Image>().enabled = false;
                RightHook.GetComponent<Image>().enabled = true;
                LeftHook.GetComponent<Image>().enabled = true;
            }
			if (startTimer > 2)
			{
	            if (Input.GetKeyDown(KeyCode.Joystick1Button0))
	            {
	                if (Selection == 0)
	                {
	                    Application.LoadLevel(Application.loadedLevel + 1);
	                }
	                if (Selection == 1)
	                {
                    //Level selection
	                    Application.LoadLevel(1);
	                }

	            }
			}
        
        
    }
    void PlayerWin(string playerWon)
    {
        startTimer = 0;

        if (playerWon == "PlayerOne")
        {
            WplayerWon.GetComponent<Image>().sprite = SpriteOrangeWon;
        }
        if (playerWon == "PlayerTwo")
        {
            WplayerWon.GetComponent<Image>().sprite = SpriteGreenWon;
        }
    }

}
