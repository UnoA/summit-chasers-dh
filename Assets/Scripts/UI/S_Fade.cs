﻿using UnityEngine;
using System.Collections;

public class S_Fade : MonoBehaviour {

    public Texture2D fadeOutTexture;
    public float fadeSpeed = 0.8f;

    private int drawdepth = 1000;
    private float alpha = 1.0f;
    private int fadeDir = -1;

    void OnGUI(){

        // deltatime ???

       alpha += fadeDir * fadeSpeed * Time.deltaTime;
        
        alpha = Mathf.Clamp01(alpha);
 
        GUI.color = new Color (GUI.color.r, GUI.color.g, GUI.color.b , alpha);
        GUI.depth = drawdepth;
        GUI.DrawTexture (new Rect (0,0, Screen.width, Screen.height), fadeOutTexture);
    }

    public float beginFade(int direction){

        fadeDir = direction;
        return (fadeSpeed);
    }

    void OnLevelWasLoaded()
    {
        beginFade(-1);
    }
}
