﻿using UnityEngine;
using System.Collections;

public class S_UIMapLengthIndicators : MonoBehaviour {

	public Transform player;
	public Transform finishLine;
    public RectTransform canvasTarget;
    private RectTransform canvas;
    private RectTransform m_transform;

	public float yOffset;
	public float xOffset;

	private float canvasHeight;
	private float mapLength;
	private float mapFullLength;
	private float percentLeft;
	
    private Vector2 newPosition;
	private Vector2 currentPosition;

	void Start()
	{
		canvas = GetComponentInParent<RectTransform> ();
		m_transform = GetComponent<RectTransform> ();
		
		mapFullLength = finishLine.position.y - player.position.y;
        canvasHeight = canvasTarget.anchoredPosition.y *2;
	}
	void Update () 
	{
        mapLength = finishLine.position.y - player.position.y;
        percentLeft = mapLength / mapFullLength;

        currentPosition = new Vector2(xOffset, ((1 - percentLeft) * canvasHeight) + yOffset);
        
        if (currentPosition.y > newPosition.y)
        {
            if (m_transform.anchoredPosition.y < canvasHeight)
            {
                m_transform.anchoredPosition = new Vector2(xOffset, ((1 - percentLeft) * canvasHeight) + yOffset);
                newPosition = m_transform.anchoredPosition;
            }
        }
	}
}




