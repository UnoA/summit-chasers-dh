﻿using UnityEngine;
using System.Collections;

public class S_Debug : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Debug.Log("Levelcount: "+Application.levelCount);
        Debug.Log("Current level: " + Application.loadedLevel);
	}
	
	// Update is called once per frame
	void Update () {
        Debug.Log("Levelcount: " + Application.levelCount);
        Debug.Log("Current level: " + Application.loadedLevel);
    }
}
