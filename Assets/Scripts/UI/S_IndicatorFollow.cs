﻿using UnityEngine;
using System.Collections;

public class S_IndicatorFollow : MonoBehaviour {
	
	public Transform target = null;
	public Vector3 offset = Vector3.zero;
	private Transform m_transform;
	
	private Camera m_camera;
	void Awake()
	{
		m_transform = GetComponent<Transform> ();
		m_camera = GetComponent<Camera>();
	}
	
	// LateUpdate is called after Update(). Here it's used to make sure that the movement done Update() has been completed before moving the camera.
	void LateUpdate () {
		

		
		if (target != null)
		{
			m_transform.position = target.position + offset;
		}
	}
}
