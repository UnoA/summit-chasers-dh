﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class S_GuiArrow : MonoBehaviour
{

    public Canvas canvas;
    public Image TargetSprite;
    public Image OnScreenSprite; // can use gameobject with spriterenderer, or reference a Sprite here, and create a new gameobject, add a sprite renderer to it.
    public Image OffScreenSprite;
    public bool displayOffscreen;

    private Image targetSprite;
    private Image onSprite;
    private Image offSprite;
    private Vector3 offScreen = new Vector3(-100, -100, 0);

    private Rect centerRect;
    Vector3 screenCenter;
    public Camera targetCamera;
    // Use this for initialization
    void Start()
    {
        screenCenter = new Vector3(targetCamera.pixelWidth, targetCamera.pixelHeight, 0) * .5f;

        onSprite = Instantiate(OnScreenSprite, offScreen, Quaternion.Euler(new Vector3(0, 0, 0))) as Image;
        offSprite = Instantiate(OffScreenSprite, offScreen, Quaternion.Euler(new Vector3(0, 0, 0))) as Image;
        onSprite.rectTransform.parent = canvas.transform;
        offSprite.rectTransform.parent = canvas.transform;
        centerRect.width = targetCamera.pixelWidth / 2;
        centerRect.height = targetCamera.pixelHeight / 2;
        centerRect.position = new Vector2(screenCenter.x - centerRect.width / 2, screenCenter.y - centerRect.height / 2);
    }

    // Update is called once per frame
    void Update()
    {
        PlaceIndicators();
    }


    void PlaceIndicators()
    {
        //GameObject[] objects = GameObject.FindObjectsOfType(typeof(AI)) as GameObject[]; //find objects by type (might ahve to find by gameobejct, and then filter for AI)
        //go through all the objects we care about

        Vector3 screenpos = targetCamera.WorldToScreenPoint(this.transform.position);


        //if onscreen
        if (screenpos.z > 0 && screenpos.x < targetCamera.pixelWidth && screenpos.x > 0 && screenpos.y < Screen.height && screenpos.y > 0)
        {
            offSprite.rectTransform.position = offScreen;//get rid of the arrow indicator

        }
        else
        {
            if (displayOffscreen)
            {
                PlaceOffscreen(screenpos);
            }
            onSprite.rectTransform.position = offScreen; //get rid of the onscreen indicator
            if(targetSprite != null)
            targetSprite.rectTransform.position = offScreen;
        }


    }

    void PlaceOffscreen(Vector3 screenpos)
    {
        float x = screenpos.x;
        float y = screenpos.y;
        float offset = 30;

        if (screenpos.z < 0)
        {
            screenpos = -screenpos;
        }

        if (screenpos.x > targetCamera.pixelWidth) 
        {
            x = targetCamera.pixelWidth - offset;
        }
        if (screenpos.x < 0.5)
        {
            x = offset;
        }

        if (screenpos.y > Screen.height)
        {
            y = Screen.height - offset;
        }
        if (screenpos.y < 0)
        {
            y = offset;
        }

        Vector3 offscreenPos = new Vector3(x, y, 0);
        offSprite.rectTransform.position = offscreenPos;

    }
}

