﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class FinishCountdown : MonoBehaviour
{
    public Image P1ReadyImage;
    public Image P2ReadyImage;
    public Transform FinishLineReady;
    private GameObject PlayerOneTransform;
    private Transform PlayerTwoTransform;
    float fuskTimer = -9999999;
	private GameObject WinBlur;
	public Text goalTimerObject;
	public AudioClip winSound;
    public GameObject Player1Background;
    public GameObject Player2Background;
    string seconds;
	string winner;
	
    // Use this for initialization
    void Start()
    {
        PlayerOneTransform = GameObject.FindGameObjectWithTag("Player");
        PlayerTwoTransform = GameObject.FindGameObjectWithTag("Player 2").transform;
		WinBlur = GameObject.Find ("P_Winblur");
		WinBlur.SetActive (false);
    }

    // Update is called once per frame
    void Update()
    {
        fuskTimer -= Time.deltaTime;
        if (PlayerOneTransform.transform.position.y > FinishLineReady.position.y )
        {
            if(P1ReadyImage != null)
            P1ReadyImage.gameObject.SetActive(true);
            if(Player1Background != null)
            Player1Background.gameObject.SetActive(true);
			if(fuskTimer < 0 && winner == null)
			{
            	fuskTimer = 6;
				AudioSource.PlayClipAtPoint(winSound, transform.position);
				winner = "PlayerOne";
			}

        }
        if (PlayerTwoTransform.position.y > FinishLineReady.position.y)
        {
            if (P2ReadyImage != null)
                P2ReadyImage.gameObject.SetActive(true);
            if (Player2Background != null)
                Player2Background.gameObject.SetActive(true);

            if (fuskTimer < 0 && winner == null)
			{
				fuskTimer = 6;
				AudioSource.PlayClipAtPoint(winSound, transform.position);
				winner = "PlayerTwo";
			}

        }
        if (winner == "PlayerTwo")
        {
            seconds = Mathf.Floor(fuskTimer % 60).ToString("0");
            goalTimerObject.gameObject.SetActive(true);
            goalTimerObject.text = seconds;
            goalTimerObject.alignment = TextAnchor.UpperLeft;
        }
        if (winner == "PlayerOne")
        {
            seconds = Mathf.Floor(fuskTimer % 60).ToString("0");
            goalTimerObject.gameObject.SetActive(true);
            goalTimerObject.text = seconds;
            goalTimerObject.alignment = TextAnchor.UpperRight;
        }
        if (PlayerOneTransform.transform.position.y > FinishLineReady.position.y && winner == "PlayerTwo")
        {
            SetSeconds();

        }
        if (PlayerTwoTransform.transform.position.y > FinishLineReady.position.y && winner == "PlayerOne")
        {
            SetSeconds();

        }
        if (PlayerOneTransform.transform.position.y > FinishLineReady.position.y && PlayerTwoTransform.position.y > FinishLineReady.position.y)
        {
            SetSeconds();

        }
        if (seconds == "0")
        {
            if(P1ReadyImage != null)
            	Destroy(P1ReadyImage.gameObject);
            if(P2ReadyImage != null)
				Destroy(P2ReadyImage.gameObject);
			Destroy(goalTimerObject.gameObject);

			WinBlur.SetActive(true);
			WinBlur.SendMessage("PlayerWin", winner);
            Player1Background.gameObject.SetActive(true);
            Player2Background.gameObject.SetActive(true);

        }

    }
    void SetSeconds()
    {
        seconds = "0";
    }
}
