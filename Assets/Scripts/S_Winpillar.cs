﻿using UnityEngine;
using System.Collections;

public class S_Winpillar : MonoBehaviour
{

    public Transform WinPillar;
    public Transform LosePillar;
    public GameObject Playerone;
    public GameObject Playertwo;
    public SpriteRenderer Beam;
    public SpriteRenderer WinHide;
    private int PlayerOneStars;
    private int PlayerTwoStars;
    private float PlayerOneTime;
    private float PlayerTwoTime;
    public GameObject winnerParticles;
    public GameObject loserParticles;
    private Animator animator;
    private bool particlePlayed;

    float timer = 0;
    bool Move = true;
    bool Winner = false;
    private bool animationOnce = true;


    void Start()
    {
        PlayerOneStars += PlayerPrefs.GetInt("LevelOnePlayerOne");
        PlayerOneStars += PlayerPrefs.GetInt("LevelTwoPlayerOne");
        PlayerOneStars += PlayerPrefs.GetInt("LevelThreePlayerOne");

        PlayerTwoStars += PlayerPrefs.GetInt("LevelOnePlayerTwo");
        PlayerTwoStars += PlayerPrefs.GetInt("LevelTwoPlayerTwo");
        PlayerTwoStars += PlayerPrefs.GetInt("LevelThreePlayerTwo");

        PlayerOneTime+= PlayerPrefs.GetInt("LevelOnePlayerOneTime");
        PlayerOneTime += PlayerPrefs.GetInt("LevelTwoPlayerOneTime");
        PlayerOneTime += PlayerPrefs.GetInt("LevelThreePlayerOneTime");

        PlayerTwoTime += PlayerPrefs.GetInt("LevelOnePlayerTwoTime");
        PlayerTwoTime += PlayerPrefs.GetInt("LevelTwoPlayerTwoTime");
        PlayerTwoTime += PlayerPrefs.GetInt("LevelThreePlayerTwoTime");

        if (PlayerOneStars != PlayerTwoStars)
        {
            if (PlayerOneStars > PlayerTwoStars)
                Winner = true;
            else
                Winner = false;
        }
        else
        {
            if(PlayerOneTime > PlayerTwoTime)
            {
                PlayerTwoStars += 1;
            }
            else
            {
                PlayerOneStars += 1;
            }
        }
      
        
        if (Winner == false)
        {
            GameObject x = Playerone;
            Playerone = Playertwo;
            Playertwo = x;

            Vector3 y = WinPillar.position;
            WinPillar.position = LosePillar.position;
            LosePillar.position = y;

            Playertwo.GetComponent<Animator>().SetBool("Cry", true);

            Beam.transform.position = new Vector3(4,Beam.transform.position.y,Beam.transform.position.z);
            WinHide.transform.position = new Vector3(4, WinHide.transform.position.y, WinHide.transform.position.z);

        }
        else
        {
            Playertwo.GetComponent<Animator>().SetBool("Cry", true);
        }
        animator = Playerone.GetComponent<Animator>();



 
    }

    void Update()
    {
        timer += Time.deltaTime;
        
        if(timer >= 1){

            float WinPillarFinal = Mathf.Lerp(WinHide.color.a, 0, Time.deltaTime * 10);
            WinHide.color = new Color(255, 255, 255, WinPillarFinal);
            if(!particlePlayed)
            {
                Instantiate(winnerParticles, new Vector3(WinPillar.position.x, -5, -2), Quaternion.Euler(0, 0, 0));
                Instantiate(loserParticles, new Vector3(LosePillar.position.x, -5, -2), Quaternion.Euler(0, 0, 0));
                particlePlayed = true;
            }
   
        }

        if (timer >= 1.5)
        {




            //win Pillar
            float pos = Mathf.Lerp(WinPillar.position.y, -2f, Time.deltaTime);
            WinPillar.position = new Vector2(WinPillar.position.x, pos);

            //Loose Pillar
            float pos2 = Mathf.Lerp(LosePillar.position.y, -5f, Time.deltaTime);
            LosePillar.position = new Vector2(LosePillar.position.x, pos2);

            //Beam
            float beamalpha = Mathf.Lerp(Beam.color.a, 1, Time.deltaTime);
            Beam.color = new Color(255, 255, 255, beamalpha);

            if (Move == true)
            {

            
                    //win PLayer
                    float pos3 = Mathf.Lerp(Playerone.transform.position.y, 1.1f, Time.deltaTime);
                    Playerone.transform.position = new Vector3(Playerone.transform.position.x, pos3, Playerone.transform.position.z);

                    //loose PLayer
                    float pos4 = Mathf.Lerp(Playertwo.transform.position.y, -1.9f, Time.deltaTime);
                    Playertwo.transform.position = new Vector3(Playertwo.transform.position.x, pos4, Playertwo.transform.position.z);


            }
        }



        if (timer >= 7)
        {



            if (animationOnce == true)
            {
                animator.SetTrigger("hover");
                animationOnce = false;
            }

            Move = false;

           float pos3 = Mathf.Lerp(Playerone.transform.position.y, 15f, Time.deltaTime/20);
           Playerone.transform.position = new Vector3(Playerone.transform.position.x, pos3, Playerone.transform.position.z);
            
            
      
        }
        if (timer >= 17)
        {
            Application.LoadLevel(Application.loadedLevel+1);
        }


    }
}