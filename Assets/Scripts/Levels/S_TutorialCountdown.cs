﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class S_TutorialCountdown : MonoBehaviour {
    float TutorialTimer;
    public Image TutorialImage;
    public Image P1ReadyImage;
    public Image P2ReadyImage;
    public Transform FinishLineReady;
    private GameObject PlayerOneTransform;
    private Transform PlayerTwoTransform;
    public Image BackgroundFade;
	// Use this for initialization
	void Start () {
        TutorialImage.gameObject.SetActive(true);
        PlayerOneTransform = GameObject.FindGameObjectWithTag("Player");
        PlayerTwoTransform = GameObject.FindGameObjectWithTag("Player 2").transform;
    }
	
	// Update is called once per frame
	void Update () {
        TutorialTimer += Time.deltaTime;
        if(TutorialImage != null)
        {
            if (TutorialTimer > 2)
                TutorialImage.color = new Color(1, 1, 1, Mathf.Lerp(TutorialImage.color.a, 0, Time.deltaTime * 2));
            if (TutorialImage.color.a <= 0.05f)
            {
                Destroy(TutorialImage.gameObject);
            }
            if (TutorialTimer > 2)
            BackgroundFade.color = new Color(0, 0,0, Mathf.Lerp(BackgroundFade.color.a, 0, Time.deltaTime * 2));
            if (BackgroundFade.color.a <= 0.01f)
            {
                Destroy(BackgroundFade.gameObject);
            }
        }
       
        if(PlayerOneTransform.transform.position.y > FinishLineReady.position.y)
        {
            P1ReadyImage.gameObject.SetActive(true);
        }
        if (PlayerTwoTransform.position.y > FinishLineReady.position.y)
        {
            P2ReadyImage.gameObject.SetActive(true);
        }
	}
}
