﻿using UnityEngine;
using System.Collections;

public class S_StartTimer : MonoBehaviour {
    float timer;
    public Font m_font;
    public Texture2D texture;
    float deltatime = 0.016f;
    public Texture2D Timer1;
    public Texture2D Timer2;
    public Texture2D Timer3;
    public Texture2D TimerGo;
	// Use this for initialization
	void Start () {
        timer = 4;

	}
	
	// Update is called once per frame
	void Update () {
        timer -= deltatime;

        if (timer <= 0)
        {
            Time.timeScale = 1;
            Destroy(this.gameObject);
        }
        if (timer <= 3.8f && timer >= 2)
        {
            Time.timeScale = 0;
        }
        

	}
    void OnGUI()
    {
        GUI.color = new Color(0, 0, 0, 1);
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), texture);

        GUI.skin.label.font = m_font;
        GUI.skin.label.fontSize = 200;
        GUI.color = new Color(1, 1, 1, 1);

        if (timer >= 3 && timer <= 4)
        {
            GUI.DrawTexture(new Rect(Screen.width/4, Screen.height/4, Screen.width/2, Screen.height/2), Timer3);
        }
        if (timer >= 2 && timer <= 3)
        {
            GUI.DrawTexture(new Rect(Screen.width/4, Screen.height/4, Screen.width / 2, Screen.height / 2), Timer2);

        }
        if (timer >= 1 && timer <= 2)
        {
            GUI.DrawTexture(new Rect(Screen.width / 4, Screen.height / 4, Screen.width / 2, Screen.height / 2), Timer1);

        }
        if (timer <= 1)
        {
            GUI.DrawTexture(new Rect(Screen.width / 4, Screen.height / 4, Screen.width / 2, Screen.height / 2), TimerGo);

        }
    }
}
