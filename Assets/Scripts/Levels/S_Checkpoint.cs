﻿using UnityEngine;
using System.Collections;

public class S_Checkpoint : MonoBehaviour {
    [Tooltip("The number of prefab spawned.")]
	public int numBlocks;
    [Tooltip("The prefab spawned.")]
    public GameObject checkpointBlock;
    [Tooltip("PlayerOne's Symbol GameObject.")]
    public GameObject p1;
    [Tooltip("PlayerTwo's Symbol GameObject.")]
    public GameObject p2;
    [Tooltip("The position of the checkpoint")]
    public Transform checkpointPosition;
    private Vector3 spawnPositionx;
    private Vector3 spawnPositionxx;
    private Vector2 playerPosition;
    private bool playerOne;
    private bool playerTwo;
    private int count = 0;
    private float timer;
    private float timeLimit = 1.2f;
    private bool p1Reach = true;
    private bool p2Reach =true;
    [Tooltip("If set to true, this checkpoint will trigger Winstate.")]
    public bool WinCheckpoint;
    [Tooltip("WinBox gameobject")]
    private GameObject FinishText;
    private Vector3 PlayerOnePos;
    private Vector3 PlayerTwoPos;
    void Start () {
        spawnPositionx = checkpointPosition.position;
        spawnPositionxx = checkpointPosition.position;
        spawnPositionx.z = 5;
        spawnPositionxx.z = 5;
        spawnPositionxx.y -= 0;
        spawnPositionx.y -= 0;
        if(WinCheckpoint)
        {
            FinishText = GameObject.Find("P_Finish");

        }

        PlayerOnePos.Set(0, -50, 0);
        PlayerTwoPos.Set(0, -50, 0);

        p2 = (GameObject)Instantiate(p2, new Vector3(GetComponentInChildren<SpriteRenderer>().transform.position.x, GetComponentInChildren<SpriteRenderer>().transform.position.y, 0), Quaternion.Euler(0, 0, 0));
        p1 = (GameObject)Instantiate(p1, new Vector3(GetComponentInChildren<SpriteRenderer>().transform.position.x, GetComponentInChildren<SpriteRenderer>().transform.position.y, 0), Quaternion.Euler(0, 0, 0));
        //p2.SetActive(false);
        //p1.SetActive(false);

    }

    // Update is called once per frame
    void Update () {
        timer += Time.deltaTime;
        if (!WinCheckpoint)
        {
            if (playerOne && playerTwo && timer > timeLimit && count < numBlocks)
            {   
			    GetComponent <AudioSource> ().Play() ;  
                Instantiate(checkpointBlock, spawnPositionx, Quaternion.Euler(0, 0, 0));
                spawnPositionx.x -= 2;
                Instantiate(checkpointBlock, spawnPositionxx, Quaternion.Euler(0, 0, 0));
                spawnPositionxx.x += 2;
                count++;
                timer = 0;
                timeLimit -= 0.1f;
            }
        }
        if (WinCheckpoint)
        {
            if (playerOne || playerTwo)
            {
                if (count < numBlocks)
                {
                    GetComponent<AudioSource>().Play();
                    Instantiate(checkpointBlock, spawnPositionx, Quaternion.Euler(0, 0, 0));
                    spawnPositionx.x -= 2;
                    Instantiate(checkpointBlock, spawnPositionxx, Quaternion.Euler(0, 0, 0));
                    spawnPositionxx.x += 2;
                    count++;
                    timer = 0;
                    timeLimit -= 0.1f;
                }
                
            }
        }
        
        if (playerOne)
        {
            p1.SetActive(true);
        }
        else 
        {
            p1.SetActive(false);
        }

        if (playerTwo)
        {
            p2.SetActive(true);
        }
        else
        {
            p2.SetActive(false);
        }


        if (PlayerOnePos.y > transform.position.y)
        {
            playerOne = true;
        }
        else
            playerOne = false;

        if (PlayerTwoPos.y > transform.position.y)
        {
            playerTwo = true;
        }
        else
            playerTwo = false;
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if (!WinCheckpoint)
        {

        }
        else
        {

            if (other.transform.tag == "Player 2")
            {
                //playerTwo = true;
              
                FinishText.gameObject.SetActive(true);

            }
            if (other.transform.tag == "Player")
            {
                //playerOne = true;

                FinishText.gameObject.SetActive(true);

            }

        }
        
    }
    void OnTriggerStay2D(Collider2D other)
    {
        if (other.transform.tag == "Player")
        {
            PlayerOnePos = other.transform.position;
        }
        else if (other.transform.tag == "Player 2")
        {
            PlayerTwoPos = other.transform.position;
        }
    }
    
}
