﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class S_ShockwaveXbox : MonoBehaviour
{

    public float cooldown;
    public float force;

    private float timer;
    private KeyCode Shock;
    private KeyCode extraShock;
    private string playerType;
    private bool playerInRange;
    private Vector3 otherPlayerPos;

    private PointEffector2D m_pointEffector;
    private ParticleSystem m_particleSystem;
    private Animator m_animator;

    private GameObject otherPlayer;
    private GameObject screenShake;

    private AudioSource m_audiosource;
    public AudioClip[] shockwaveSoundEffects;
    void Start()
    {

        m_pointEffector = GetComponent<PointEffector2D>();
        m_particleSystem = GetComponent<ParticleSystem>();
        m_animator = GetComponentInParent<Animator>();
        playerType = GetComponentInParent<S_PlayerMovement>().playerType;
        m_audiosource = GetComponent<AudioSource>();

        if (playerType == "PlayerTwo")
        {
            Shock = KeyCode.Joystick2Button4;
            extraShock = KeyCode.Joystick2Button5;
            otherPlayer = GameObject.FindWithTag("Player");
            screenShake = GameObject.FindWithTag("PlayerTwoCamera");
        }

        else if (playerType == "PlayerOne")
        {
            Shock = KeyCode.Joystick1Button4;
            extraShock = KeyCode.Joystick1Button5;
            otherPlayer = GameObject.FindWithTag("Player 2");
            screenShake = GameObject.FindWithTag("PlayerOneCamera");
        }

        timer = 1;
    }

    void Update()
    {
        timer += Time.deltaTime;

        if (timer > cooldown && !GetComponentInParent<S_PlayerMovement>().isHit)
        {
            if (Input.GetKeyDown(Shock) || Input.GetKeyDown(extraShock))
            {


                m_pointEffector.forceMagnitude = force;
                m_particleSystem.Play();
                m_audiosource.PlayOneShot(shockwaveSoundEffects[Random.Range(0, shockwaveSoundEffects.Length)]);
                timer = 0;
                m_animator.SetBool("Explode", true);

                if (playerInRange)
                {
                    otherPlayer.GetComponent<S_PlayerMovement>().HitByShockwave();
                    screenShake.SendMessage("setShake", 100f);
                    gameObject.GetComponentInParent<S_PlayerMovement>().FreezeTime();

                }
            }
        }
        else
        {
            m_pointEffector.forceMagnitude = 0;

        }
        if (timer > 0.5f)
            m_animator.SetBool("Explode", false);

    }
    void OnTriggerStay2D(Collider2D col)
    {
        if (playerType == "PlayerOne" && col.gameObject.tag == "Player 2")
        {
            playerInRange = true;
            otherPlayerPos = col.gameObject.transform.position;
        }

        if (playerType == "PlayerTwo" && col.gameObject.tag == "Player")
        {
            playerInRange = true;
            otherPlayerPos = col.gameObject.transform.position;
        }
    }
    void OnTriggerExit2D(Collider2D col)
    {
        if (playerType == "PlayerOne" && col.gameObject.tag == "Player 2")
            playerInRange = false;

        if (playerType == "PlayerTwo" && col.gameObject.tag == "Player")
            playerInRange = false;
    }
}