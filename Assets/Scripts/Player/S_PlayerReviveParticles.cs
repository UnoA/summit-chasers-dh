﻿using UnityEngine;
using System.Collections;

public class S_PlayerReviveParticles : MonoBehaviour {

    private Vector3 playerPos;
	
	void Update()
	{
		if(playerPos.x != 0 && playerPos.y != 0)
			transform.position = playerPos;
		transform.rotation = Quaternion.Euler (0, 0, 0);
	}
	void OnTriggerStay2D(Collider2D col)
	{
        if (col.gameObject.tag == "Player 2" || col.gameObject.tag == "Player")
			playerPos = new Vector3 (col.gameObject.transform.position.x, col.gameObject.transform.position.y, -9f);
	}
}
