﻿using UnityEngine;
using System.Collections;

public class S_ShockWaveSoundEffects : MonoBehaviour {

	private AudioSource source;

	public AudioClip shockwave;
	public float ShockVolLowRange;
	public float ShockVolHighRange;
	public float ShockPitchLowRange;
	public float ShockPitchHighRange;

	public AudioClip destroyBlocks;
	public float volDestroyLowRange;
	public float volDestroyHighRange;
	public float destroyBlockPitchLowRange;
	public float destroyBlockHighRange;

	void Start()
	{
		source = GetComponent<AudioSource>();
	}

	public void PlayShockWave()
	{
		source.pitch = Random.Range (ShockPitchLowRange, ShockPitchHighRange);
		float vol = Random.Range (ShockVolLowRange, ShockVolHighRange);
		source.PlayOneShot (shockwave, vol);
	}
	public void PlayDestroyBlock()
	{
		source.pitch = Random.Range (destroyBlockPitchLowRange, destroyBlockHighRange);
		float vol = Random.Range (volDestroyLowRange, volDestroyHighRange);
		source.PlayOneShot (destroyBlocks, vol);
	}

}
