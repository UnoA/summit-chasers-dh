﻿using UnityEngine;
using System.Collections;

public class S_PlayerMovement : MonoBehaviour
{
    [SerializeField]
    //[HideInInspector]
    [Tooltip("The cooldown of the players ability to grab")]
    public float grabLimit = 1;
    [HideInInspector]
    public bool facingRight = true;			// For determining which way the player is currently facing.
    //[HideInInspector]
    private bool grab;
    public string playerType;
    private Vector3 m_launchDirection = Vector3.up;
    private Vector3 m_pushDirection = Vector3.up;
    public float m_pushSpeed = 0.2f;
    public float m_groundSpeed = 0.2f;
    private Rigidbody2D m_rigidbody2d = null;
    private LineRenderer m_lineRenderer = null;
    private SpriteRenderer m_spriteRenderer = null;
    public float gravity;

    private AudioSource m_audiosource;
    public AudioClip soundEffectGrab;
    public AudioClip[] soundEffectsJump;
    private bool playedSound = false;
    public GameObject grabParticles;
    public GameObject reviveParticles;
    public GameObject stunnedParticles;
    [HideInInspector]
    public bool playedParticles;

    private Transform m_transform = null;
    private float deathTimer;
    public float knockedOutTime;
    public bool isGrabbing = false;
    //[HideInInspector]
    public bool grounded = false;			// Whether or not the player is grounded.
    private bool playerCollision = false;
    private Animator m_animator = null;
    private Vector3 checkpointPosition;
    private float m_grabTimer;
    [Tooltip("The force with which the ball is launched")]
    //[SerializeField]
    public float m_maxForce = 28.0f;
    public float m_launchForce = 20.0f;
    public float m_chargeUpSpeed = 20.0f;
    public float m_chargeStartAmount = 1.4f;
    public float speed;
    public float maxSpeed = 40f;

    [Tooltip("Player radius: 0.5")]
    public float grabRadius = 1.2f;
    public float iceRadius = 0.4f;
    private bool grabRange;
    private bool onIce;
    public LayerMask whatIsGround;
    public LayerMask whatIsIce;
    // Use this for initialization

    private KeyCode LAUNCH;
    private KeyCode GRAB;

    private float Xon;
    private float Yon;
    public Vector3 pointOfContact;
    public bool isHit = false;
    Vector2 contactPos;

    private GameObject winText;
    float IceTimer = 0;
    bool GrabIce = false;
    private bool Pause = false;

    private float freezeTimer;
    public float impactFreezeTime = 0.0002f;
    private float elapsedFreezeTime;
    void Start()
    {
        m_transform = GetComponent<Transform>();
        m_rigidbody2d = GetComponent<Rigidbody2D>();
        m_lineRenderer = GetComponent<LineRenderer>();
        m_spriteRenderer = GetComponent<SpriteRenderer>();
        m_animator = GetComponent<Animator>();
        m_audiosource = GetComponent<AudioSource>();

        if (playerType == "PlayerTwo")
        {
            LAUNCH = KeyCode.Joystick2Button0;

        }
        else if (playerType == "PlayerOne")
        {
            LAUNCH = KeyCode.Joystick1Button0;
        }

    }

    // Update is called once per frame
    void Update()
    {

        if(Input.GetKeyDown(KeyCode.Joystick1Button7) || Input.GetKeyDown(KeyCode.Joystick2Button7))
        {
            Pause = !Pause;
        }
        if (!GameObject.Find("P_StartTimer") && !Pause)
        {
            deathTimer += Time.deltaTime;
            if (!isHit)
            {
                m_animator.SetBool("Grounded", grounded);
                m_grabTimer += Time.deltaTime;

                speed = Mathf.Abs(m_rigidbody2d.velocity.magnitude);

                if (m_animator.GetFloat("Speed") < 5f)
                {
                    m_rigidbody2d.fixedAngle= true;
                    m_transform.rotation = Quaternion.Euler(0, 0, 0);
                }

                else
                    m_rigidbody2d.fixedAngle = false;

                m_animator.SetFloat("Speed", speed);
                grounded = Physics2D.OverlapCircle(transform.position, 1.2f, whatIsGround);
                grabRange = Physics2D.OverlapCircle(transform.position, grabRadius, whatIsGround);
                onIce = Physics2D.OverlapCircle(transform.position, iceRadius, whatIsIce);
                
                if (speed >= 5)
                {
                    m_animator.SetBool("Grab", false);
                }
                if (speed != 0)
                {
                    grab = true;
                }
                else if (speed == 0)
                {
                    //m_transform.rotation = Quaternion.Euler(0, 0, 0);

                }
                if (speed != 0 && speed < 5)
                {
                    // m_transform.rotation = Quaternion.Euler(0, 0, 0);
                }
                if (grounded || !onIce)
                {
                    if (playerType == "PlayerOne")
                    {
                        Xon = (Input.GetAxis("Joy X") * 10);
                        Yon = (Input.GetAxis("Joy Y") * 10);
                    }
                    if (playerType == "PlayerTwo")
                    {
                        Xon = (Input.GetAxis("Joy2 X") * 10);
                        Yon = (Input.GetAxis("Joy2 Y") * 10);
                        //Debug.Log(new Vector2(Xon, Yon).normalized);
                    }
                    m_launchDirection = new Vector3(Xon, Yon);

                    if (Xon > 0.25 && !isGrabbing)
                    {
                        m_pushDirection = new Vector3(1, 0);
                        if (m_rigidbody2d.velocity.x < 0 && grounded)
                        {
                            m_rigidbody2d.AddForce(m_pushDirection.normalized * m_groundSpeed, ForceMode2D.Impulse);
                        }
                        else if (!grounded)
                        {
                            m_rigidbody2d.AddForce(m_pushDirection.normalized * m_pushSpeed, ForceMode2D.Impulse);
                        }
                        else if (Mathf.Abs(m_rigidbody2d.velocity.x) < 2.0f)
                        {
                            m_rigidbody2d.AddForce(m_pushDirection.normalized * m_groundSpeed, ForceMode2D.Impulse);
                        }

                    }
                    if (Xon < -0.25 && !isGrabbing)
                    {
                        m_pushDirection = new Vector3(1, 0);
                        if (m_rigidbody2d.velocity.x > 0 && grounded)
                        {
                            m_rigidbody2d.AddForce(m_pushDirection.normalized * -m_groundSpeed, ForceMode2D.Impulse);
                        }
                        else if (!grounded)
                        {
                            m_rigidbody2d.AddForce(m_pushDirection.normalized * -m_pushSpeed, ForceMode2D.Impulse);
                        }
                        else if (Mathf.Abs(m_rigidbody2d.velocity.x) < 2.0f)
                        {
                            m_rigidbody2d.AddForce(m_pushDirection.normalized * -m_groundSpeed, ForceMode2D.Impulse);
                        }
                    }
                    if (Input.GetKeyDown(LAUNCH))
                    {
                        if (grounded)
                        {
                            m_lineRenderer.enabled = true;
                            m_lineRenderer.SetWidth(0.8f, 0);

                        }

                        m_launchForce = m_maxForce / m_chargeStartAmount;
                    }
                    if (Input.GetKey(LAUNCH))
                    {
                        Grab();
                        if (m_lineRenderer.enabled == false && grounded)
                            m_lineRenderer.enabled = true;
                    }
                    if (Input.GetKey(LAUNCH))
                    {
                        if (grounded)
                        {
                            isGrabbing = true;

                            if(m_launchForce < m_maxForce)
                            m_launchForce += Time.deltaTime * m_chargeUpSpeed;

                        }
                        else
                        {
                            m_lineRenderer.SetWidth(0.8f, 0);
                            m_lineRenderer.SetPosition(0, transform.position);
                            m_lineRenderer.SetPosition(1, transform.position + m_launchDirection.normalized * 3f);
                        }
                        m_lineRenderer.SetWidth(0.8f, 0);

                        m_lineRenderer.SetPosition(0, transform.position);
                        m_lineRenderer.SetPosition(1, transform.position + m_launchDirection.normalized * m_launchForce/8);
                    }
                    //Launch
                    if (Input.GetKeyUp(LAUNCH))
                    {

                        if (grounded)
                        {
                            isGrabbing = false;
                            playedSound = false;
                            m_rigidbody2d.gravityScale = gravity;
                            if (m_grabTimer > grabLimit)
                            {
                                m_grabTimer = 0;
                                m_rigidbody2d.AddForce(m_launchDirection.normalized * m_launchForce, ForceMode2D.Impulse);
                                Instantiate(grabParticles, new Vector3(m_transform.position.x, m_transform.position.y, 2), Quaternion.Euler(0f, 0f, 0f));


                                m_audiosource.pitch = Random.Range(0.9f, 1.3f);
                                float vol = Random.Range(0.8f, 1.2f);
                                m_audiosource.PlayOneShot(soundEffectsJump[Random.Range(0, soundEffectsJump.Length)], vol);

                            }
                        }
                        m_lineRenderer.enabled = false;
                        m_lineRenderer.SetWidth(0f, 0f);
                        m_rigidbody2d.WakeUp();
                        m_animator.SetBool("Grab", false);
                        m_rigidbody2d.gravityScale = gravity;
                        grab = false;

                    }
                }
            }
            if (contactPos.x < m_transform.position.x && m_animator.GetBool("Grab") && !facingRight)
                Flip();
            if (!isGrabbing)
            {
                if (m_rigidbody2d.velocity.x > 1 && !facingRight)
                {
                    Flip();

                }
                if (m_rigidbody2d.velocity.x < -1 && facingRight)
                {
                    Flip();
                }
            }
            if (isHit)
            {
                if (!m_animator.GetBool("Hit"))
                    m_animator.SetBool("Hit", true);
                m_lineRenderer.SetWidth(0f, 0f);
            }
            if (isHit && deathTimer > knockedOutTime)
            {
                m_animator.SetBool("Hit", false);
                isHit = false;
                playedParticles = false;
            }
            if (!grounded)
                isGrabbing = false;
            if (!isGrabbing || !grounded)
            {
                m_rigidbody2d.gravityScale = gravity;
            }
            if (m_rigidbody2d.velocity.magnitude > maxSpeed)
            {
                m_rigidbody2d.velocity = m_rigidbody2d.velocity.normalized * maxSpeed;
            }
        }

        freezeTimer = Time.realtimeSinceStartup / 1000;
        if (freezeTimer - elapsedFreezeTime > impactFreezeTime)
        {
			if(!Pause && !GameObject.Find("P_StartTimer"))
            Time.timeScale = 1;
        }
        else
            Debug.Log("TIME IS FROZEN");
    }


    void Grab()
    {
        if (grabRange && grab == true)
        {
            RaycastHit2D rotationDirection = Physics2D.CircleCast(transform.position, grabRadius, Vector2.zero, 0, whatIsGround);
            if (rotationDirection.point == new Vector2(0, 0))
            {
                rotationDirection = Physics2D.CircleCast(transform.position, grabRadius + 0.1f, Vector2.zero, 0, whatIsGround);
            }

            //transform.rotation = Quaternion.FromToRotation(Vector3.left, rotationDirection.normal);
            Vector3 direction = new Vector3(rotationDirection.point.x, rotationDirection.point.y, 0) - this.transform.position;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
            Debug.DrawRay(this.transform.position, direction, Color.red, 50);

            if (!facingRight)
            {
                facingRight = true;
                // Multiply the player's x local scale by -1.
                Vector3 theScale = transform.localScale;
                theScale.x *= -1;
                transform.localScale = theScale;
            }

            /////////////////
            if (!playedSound)
            {
                m_audiosource.pitch = Random.Range(0.6f, 0.9f);
                float vol = Random.Range(0.8f, 1.2f);
                m_audiosource.PlayOneShot(soundEffectGrab, vol);
                playedSound = true;
                Instantiate(grabParticles, transform.position, Quaternion.Euler(0, 0, 0));
            }

            m_rigidbody2d.gravityScale = 0;
            m_rigidbody2d.Sleep();
            m_animator.SetBool("Grab", true);
        }
        else
        {
            m_animator.SetBool("Grab", false);
        }

    }
    public void Respawn()
    {
        transform.position = checkpointPosition;
    }
    public void SetCheckPoint(Vector3 pos)
    {
        checkpointPosition = pos;
    }
    void Flip()
    {
        // Switch the way the player is labelled as facing.
        facingRight = !facingRight;

        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }


    void OnCollisionStay2D(Collision2D collider)
    {
        contactPos = collider.contacts[0].point;
        if (Input.GetKey(LAUNCH))
        {

            pointOfContact = collider.contacts[0].point;


            Vector2 dir = -collider.contacts[0].normal;
            //var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            if (collider.gameObject.layer == 11)
                Debug.DrawRay(this.transform.position, dir, Color.blue, 50);

        }

    }

    public void HitByShockwave()
    {
        deathTimer = 0;
        isHit = true;
        m_rigidbody2d.gravityScale = gravity;
        Instantiate(stunnedParticles, transform.position, Quaternion.Euler(0, 0, 0));
        //if (winText != null)
        //{
        //    winText.SendMessage("StealPickUps", playerType);
        //    winText.SendMessage("Shockwave", playerType);
        //}
        elapsedFreezeTime = Time.realtimeSinceStartup /1000;
        freezeTimer = 0f;
        Time.timeScale = 0.000001f;
        //Debug.Log("isHit");
        //Debug.Log(playerType);
        Debug.Log(Time.timeScale + "Timescale");
        Debug.Log(freezeTimer + "FREEZETIMER");
        
    }
    public void FreezeTime()
    {
        elapsedFreezeTime = Time.realtimeSinceStartup / 1000;
        freezeTimer = 0;
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(contactPos, 0.1f);
    }
}
