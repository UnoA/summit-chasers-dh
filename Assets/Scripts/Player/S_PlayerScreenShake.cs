﻿using UnityEngine;
using System.Collections;

public class S_PlayerScreenShake : MonoBehaviour {

    private GameObject shake;
    public float minForceToShake;
	
	void Start ()
    {
        if (gameObject.tag == "Player")
        {
            shake = GameObject.FindGameObjectWithTag("PlayerOneCamera");
        }
        if (gameObject.tag == "Player 2")
        {
            shake = GameObject.FindGameObjectWithTag("PlayerTwoCamera");
        }
    }
	void OnCollisionEnter2D(Collision2D col)
	{
		if (col.relativeVelocity.magnitude > minForceToShake) 
		{
			shake.SendMessage("setShake", col.relativeVelocity.magnitude);
		}
	}
}
