﻿using UnityEngine;
using System.Collections;

public class S_FollowObject : MonoBehaviour {

    public Transform target = null;
    public Vector3 offset = Vector3.zero;

    private Transform m_transform = null;
    private Camera m_camera;
    private float timer;

    private GameObject winText;

    public float minForce = 10f;
    public float maxForce = 25f;
	private float shakeAmount;
	private bool isShake;
	private float shakeTimer = 0;
	private float shakeForce;
	private float startShakeAmount;
    void Awake()
    {
        m_transform = GetComponent<Transform>();
        m_camera = GetComponent<Camera>();
		startShakeAmount = shakeAmount;
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 60;

    }

    void LateUpdate ()
    {
		if (isShake)
        {
            shakeAmount = Mathf.MoveTowards(shakeAmount, 0, Time.deltaTime*2);

            m_camera.transform.position = new Vector3(
                target.position.x + offset.x + Random.insideUnitCircle.x * shakeAmount, 
                target.position.y + offset.y + Random.insideUnitCircle.y * shakeAmount, 
                m_camera.transform.position.z);
              
            shakeTimer += Time.deltaTime;

            if (shakeTimer > 0.2)
            {
            	isShake = false;
            	shakeTimer = 0;
            	shakeAmount = startShakeAmount;
            }
		}

		timer += Time.deltaTime;
        
        if (target != null && !isShake)
        {
            m_transform.position = target.position + offset;
        }
        if (winText != null)
        winText.SendMessage("SetTime", timer);
	}
	public void setShake(float shakeForce)
	{
        if (shakeForce == 100f)
            shakeAmount = 0.5f;
        else
        {
            if (shakeForce > maxForce)
                shakeAmount = 0.35f;
            else if (shakeForce < maxForce && shakeForce > minForce)
                shakeAmount = 0.2f;
            else if (shakeForce < minForce)
                shakeAmount = 0.15f;
        }
       
        shakeTimer = 0;
		isShake = true;
	}
    void OnGUI()
    {

        string minutes = Mathf.Floor(timer / 60).ToString("00");
        string seconds = Mathf.Floor(timer % 60).ToString("00");
        GUI.skin.box.fontSize = 90;
        //GUI.Box(new Rect(20, 20, 250, 100), minutes + ":" + seconds);
    }
}
